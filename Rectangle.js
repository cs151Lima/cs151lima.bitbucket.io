'use strict'

class Rectangle {
	constructor() {
		this.x = 0
		this.y = 0
		this.width = 0
		this.height = 0
	}
	setRect(x, y, width, height){
		this.x = x
		this.y = y
		this.width = width
		this.height = height
	}
	
	getWidth(){
		return this.width
	}
	getHeight(){
		return this.height
	}
	getX(){
		return this.x
	}
	getY(){
		return this.y
	}	
	
	
}
