'use strict'

class Point {
	constructor() {
		this.x = 0
		this.y = 0
	}
	
	setPoint(x, y){
		this.x = x
		this.y = y
	}
	getX(){
		return this.x
	}
	getY(){
		return this.y
	}
}