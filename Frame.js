'use strict' 

function drawGrabber(x, y)
{
  const panel = document.getElementById('graphpanel')
  const SIZE = 5
  let temp = new Rectangle()
  temp.setRect(x - SIZE / 2, y - SIZE / 2, SIZE, SIZE)
  const square = document.createElementNS('http://www.w3.org/2000/svg', 'rect')
  square.setAttribute('x', temp.getX())
  square.setAttribute('y', temp.getY())
  square.setAttribute('width', temp.getWidth())
  square.setAttribute('height', temp.getHeight())
  square.setAttribute('fill', 'purple')
  panel.appendChild(square)
}

document.addEventListener('DOMContentLoaded', function () {
  const graph = new Graph()
	graph.setnodesToBeRemoved(new CircleNode())
	graph.setnodesToBeRemoved(new rectangleNode())
	graph.setnodesToBeRemoved(new diamondNode())
	graph.setedgesToBeRemoved(new LineEdge())
	graph.setnodesToBeRemoved(new LineEdge())
console.log("her")

  const toolBar = new ToolBar(graph)
  toolBar.add()
  graph.draw()
  let lastSelected = null
  let selectedButton = null
  let mouseDownPoint = null
  let lastMousePoint = null
  
  const panel = document.getElementById('graphpanel')
  let selected = null
  let dragStartPoint = null
  let dragStartBounds = null
  let DRAG_RUBBERBAND = null

  function  repaint () {
    panel.innerHTML = ''
    graph.draw()
    if (selected !== undefined) {
      const bounds = selected.getBounds()
      drawGrabber(bounds.x, bounds.y)
      drawGrabber(bounds.x + bounds.width, bounds.y)
      drawGrabber(bounds.x, bounds.y + bounds.height)
      drawGrabber(bounds.x + bounds.width, bounds.y + bounds.height)
    }
  }
  
  function mouseLocation(event) {
    var rect = panel.getBoundingClientRect()
	  let p = new Point()
	  p.setPoint(event.clientX - rect.left, event.clientY - rect.top)
	  return p  
  }

  panel.addEventListener('mouseup', event => {
    let tool = toolBar.getSelectedTool()
  if (DRAG_RUBBERBAND !== null)
  {
   let mouseUpPoint = mouseLocation(event)
   let Newproto = tool
   let newEdge = Newproto.clone()
   if (graph.connect(newEdge, 
     DRAG_RUBBERBAND, mouseUpPoint))
    selected = newEdge
  }

  repaint()

  lastMousePoint = null;
  dragStartBounds = null;
  DRAG_RUBBERBAND = null;
  lastSelected = selected;
  selected = null;
})
  
  panel.addEventListener('mousedown', event => {
     let mouseDownPoint = mouseLocation(event)
	   let node = graph.findNode(mouseDownPoint) 
	   let tool = toolBar.getSelectedTool()
	   if (tool === null) 
	   {
			 selected = node
			 dragStartPoint = mouseDownPoint
			 dragStartBounds = node.getBounds()	  
	   }   	  
		  graph.addNode(tool.clone(), mouseDownPoint)		  
			selected = newNode
			dragStartPoint = mouseDownPoint
			dragStartBounds = newNode.getBounds()
		  
		 if (node !== null)
		  {
			 selected = node
			 dragStartPoint = mouseDownPoint
			 dragStartBounds = node.getBounds()
		  }

	   lastMousePoint = mouseDownPoint
	   repaint()
  })

  panel.addEventListener('mousemove', event => {
    if (dragStartPoint === null) return
    let mouseMovPoint = mouseLocation(event)
    if (selected !== null) {
      const bounds = selected.getBounds();
      selected.translate(
        dragStartBounds.getX() - bounds.getX()
          + mouseMovPoint.getX() - dragStartPoint.getX(),
        dragStartBounds.getY() - bounds.getY() 
          + mouseMovPoint.getY() - dragStartPoint.getY());
      repaint()
    }
  })	
})
