'use strict'

class Line {
	constructor() {
		this.point1 = new Point()
		this.point2 = new Point()
	}
	
	setPoints(p1, p2){
		this.point1 = p1
		this.point2 = p2
	}
	getPoint1(){
		return this.point1
	}
	getPoint2(){
		return this.point2
	}
	getX1(){
		return this.point1.getX()
	}
	getY1(){
		return this.point1.getY()
	}
	getX2(){
		return this.point2.getX()
	}
	getY2(){
		return this.point2.getY()
	}
}