'use strict'

class ToolBar{

    constructor(graph) {
			
    this.tools = []
		this.group = graph.getnodesToBeRemoved()
		
  }
  
   add(){
   
		const div = document.getElementById('toolbar')
		const newButton = document.createElement('button')
		newButton.setAttribute("style", "background-color: thin;")
		newButton.style.width = '100px'
		newButton.style.height = '50px'
		newButton.style.backgroundImage	
	
		newButton.addEventListener('click', event => {
			let name = event.target.name
			let added = false
			for (let i = 0; i < this.group.length; i++)
			{
				if (this.group[i].constructor.name === name){
				
					this.currentTool = this.group[i]
					added = true
				}
			}
			if (!added) this.currentTool = null
		})

		const textNew = document.createTextNode("Grabber")
		newButton.appendChild(textNew)
		div.appendChild(newButton)
	    for (let i = 0; i < this.group.length; i++)
		{			
			const div = document.getElementById('toolbar')
			const newButton = document.createElement('button')
			newButton.setAttribute('name', this.group[i].constructor.name)
			newButton.setAttribute('class', 'toolbarButtons')
			newButton.setAttribute("style", "background-color: thin;")
			newButton.style.width = '100px'
			newButton.style.height = '50px'
			

			newButton.addEventListener('click', event => {
				let name = event.target.name
				for (let i = 0; i < this.group.length; i++)
				{
					if (this.group[i].constructor.name === name){
				
						this.currentTool = this.group[i]
						added = true
					}
				}
				if (!added) this.currentTool = null
			})
			const textNew = document.createTextNode(this.group[i].constructor.name)
			newButton.appendChild(textNew)
			div.appendChild(newButton)
		}
   }
   getSelectedTool()
   {
	   return this.currentTool
   }

  handleEvent(event){
        if(selectedTool !== undefined){
            if(selectedTool.getType() === "NODE"){
                const item = selectedTool.clone()
                var rect = panel.getBoundingClientRect();
                let size = item.getSize()
                item.translate( event.clientX - rect.left - size/2 ,event.clientY - rect.top - size/2)
                graph.add(item)
                graph.draw()
            }
            if(selectedTool.getType()==="EDGE"){
                let dragStartPoint = undefined
                let selected = undefined
                let edge = selectedTool.clone()
                panel.addEventListener('mousedown', event => {
                    let mousePoint = mouseLocation(event)
                    selected = graph.findNode(mousePoint)
                    if (selected !== undefined) {
                      dragStartPoint = mousePoint
                    }
                  })
                  panel.addEventListener('mousemove', event => {
                    if (dragStartPoint === undefined) return
                    let mousePoint = mouseLocation(event)
                    if (selected !== undefined) {
                      paintEdge(dragStartPoint,mousePoint)
                    }
                  })
                  panel.addEventListener('mouseup', event => {
                    let mousePoint = mouseLocation(event)
                    let connectedNode = graph.findNode(mousePoint)
                    if(connectedNode){
                        console.log("here")
                        if(graph.connect(dragStartPoint,mousePoint,edge)){
                            dragStartPoint = undefined
                            repaint()
                            edge = selectedTool.clone()
                        }
                    }else{
                        dragStartPoint = undefined
                        repaint()
                    }
                  })
function paintEdge(dragStartPoint,mousePoint){
        panel.innerHTML = ''
        const edge = document.createElementNS('http://www.w3.org/2000/svg', 'line')
        edge.setAttribute('x1', dragStartPoint.x)
        edge.setAttribute('y1', dragStartPoint.y)
        edge.setAttribute('x2', mousePoint.x)
        edge.setAttribute('y2', mousePoint.y)
        edge.setAttribute('stroke', 'black')
        edge.setAttribute('stroke-width', 2)
        panel.appendChild(edge)
        graph.draw()
      }
 
  }
}
  }
}

