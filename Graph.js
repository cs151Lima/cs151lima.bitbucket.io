'use strict'
class Graph {

   constructor(){
   
      this.nodes = []
      this.edges = []
	   this.nodesToBeRemoved = []
	   this.edgesToBeRemoved = []   
   }

  connect (edge, p1, p2) { 
   const s = this.findNode(p1)
   const e = this.findNode(p2)
   if (s && e) {
     edge.connect(s, e)
     this.edges.push(edge)
     return true
   }
   return false
 }
 findNode(p) {
   for (let i = this.nodes.length - 1; i >= 0; i--) {
     const n = this.nodes[i]
     if (n.contains(p)) return n
   }
   return undefined
 }
   findEdge(p)
   {
      for (let i = this.edges.length - 1; i >= 0; i--)
      {
         const e = this.edges[i]
         if (e.contains(p)) return e
      }
      return null
   }

   removeNode(n)
   {	  
      for (let i = 0; i < this.edges.length; i++)
      {
         let e = this.edges[i]
         if (e.getStart() === n || e.getEnd() === n)
            removeEdge(e)
      }
	  let index = this.nodes.indexOf(n)
	  this.nodes.splice(index, 1)
   }

   removeEdge(e)
   {
      this.edges.splice(this.edges.indeOf(e), 1)
   }

   getBounds()
   {
      let r = this.minBounds
      for (let i = 0; i < this.nodes.length; i++)
      {
         let n = this.nodes[i]
         let b = n.getBounds()
         if (r === null) r = b
         else r.add(b)
      }
      for (let i = 0; i < this.edges.length; i++)
      {
         let e = this.edges[i]
         r.add(e.getBounds())
      }
	  let temp = new Rectangle()
	  temp.setLocation(r.getX(), r.getY())
	  temp.setWidth(r.getWidth() +4)
	  temp.setHeight(r.getHeight() + 4)
      return (r === null ? new Rectangle() : temp)
   }

   addNode(n, p)
   {
		  let bounds = n.getBounds()
		  n.translate(p.getX() - bounds.getX(), 
		  p.getY() - bounds.getY())
		  this.nodes.push(n)
   } 

   setnodesToBeRemoved(n){
	   let duplicate = false
	   for (let i = 0; i < this.nodesToBeRemoved.length; i++){
         if (this.nodesToBeRemoved[i].constructor.name === n.constructor.name)
          duplicate = true
	   }
      if (duplicate === false) 
       this.nodesToBeRemoved.push(n)
   }
   setedgesToBeRemoved(e){
	   let duplicateEag = false
	   for (let i = 0; i < this.edgesToBeRemoved.length; i++){
         if (this.edgesToBeRemoved[i].constructor.name === e.constructor.name) 
         duplicateEag = true
	   }
      if (duplicateEag === false) 
       this.edgesToBeRemoved.push(e)
   }
   getnodesToBeRemoved(){
	   return this.nodesToBeRemoved
   }
   
   getedgesToBeRemoved(){
	   return this.edgesToBeRemoved
   }
   draw () { 
      for (const n of this.nodes) {
        n.draw(document.getElementById('graphpanel'))
      }
      for (const e of this.edges) {
        e.draw(document.getElementById('graphpanel'))
      }
    }
} 